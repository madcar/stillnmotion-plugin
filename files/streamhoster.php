<?php
add_shortcode('streamhoster', 'streamhoster_shortcode');
function streamhoster_shortcode($atts, $content = ''){
    static $count = 1;
    $atts = shortcode_atts(array(
        'width' => '640',
        'height' => '360',
        'frameborder' => '0',
        'scrolling' => 'no',
        'allowfullscreen' => 'allowfullscreen',
        'src' => ''
    ), $atts, 'streamhoster');
    $src = esc_attr($atts['src']);
    $width = (int) $atts['width'];
    $height = (int) $atts['height'];
    // $src = (!empty($atts['src']) ? $atts['src'] : false);
    if($src === ''){
        print "No source specified in streamhoster shortcode";
        return '';
    }
    $side = ($count % 2 ? 'odd' : 'even');
    $count++;
    if($content != ''){
        $content = '<p>' . $content . '</p>';
    }
    $iframe = sprintf('<iframe src="%s" width="%d" height="%d" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>', $src, $width, $height);
    //$iframe = '<iframe src="' . $src . '" width=""></iframe>';
    $output = '<div class="sh-video ' . $side . '">';
    $output .= $content; 
    $output .= $iframe;
    $output .= '</div>';
    return $output;
}
?>
