<?php
/*
Plugin Name: Still N Motion Custom
Plugin URI: https://bitbucket.org/madcar/stillnmotion-plugin
Description: Custom Plugin for Stillnmotion.com
Version: 1.0.5
Author: Andrew Taylor
Bitbucket Plugin URI: https://bitbucket.org/madcar/stillnmotion-plugin
Bitbucket Branch: master
*/
?>
<?php
$stillnmotion_plugin_path = plugins_url() . '/stillnmotion-custom';

add_filter('jetpack_development_mode', '__return_true');

add_action('init', 'init_plugin');
function init_plugin(){
    $awt_gallery = 'files/gallery.php';
    //require($awt_gallery);
    $streamhoster = plugin_dir_path(__FILE__) . 'files/streamhoster.php';
    include($streamhoster);
}

// remove_shortcode('gallery');
// add_shortcode('gallery', 'parse_gallery_shortcode');
// add_filter('wp_get_attachment_image_attributes', 'image_data_atts', 10, 2);
// $awt_id = array();
function image_data_atts($atts, $attachment){
    global $_wp_additional_image_sizes;
    $atts['data-id'] = $attachment->ID;
    $images['medium'] = wp_get_attachment_image_src($attachment->ID, 'medium');
    $images['large'] = wp_get_attachment_image_src($attachment->ID, 'large');
    $images['full'] = wp_get_attachment_image_src($attachment->ID, 'full');
    // _log('images');
    // _log($images);
    foreach($images as $size => $image){
        if($src = $images[$size][0]){
            $atts['data-src-' . $size] = $src;
        }
    }
    return $atts;
};
//    function awt_get_image_sizes(){
//        $int_sizes = get_intermediate_image_sizes();
//        $image_sizes = array();
//        foreach($int_sizes as $name){
//            if(!preg_match('/thumb/i', $name)){
//                if(in_array($name, array('thumbnail', 'medium', 'large', 'full'))){
//                    $image_sizes[$name]['width'] = get_option( $name . '_size_w' );
//                    $image_sizes[$name]['height'] = get_option( $name . '_size_h' );
//                    $image_sizes[$name]['crop'] = (bool) get_option( $name . '_crop' );
//                }
//            }
//        }
//        //_log('image sizes called');
//        return $image_sizes;
//    };
//    add_action('init', 'awt_localize_images');
//    function awt_localize_images(){
//        $image_sizes = awt_get_image_sizes();
//        wp_localize_script('main', 'imageSizes', $image_sizes);
//    }
//    add_action('wp_enqueue_scripts', 'awt_localize_images');
// _log('additional image sizes');
// _log($image_sizes);

// function parse_gallery_shortcode($atts) {
//     // awt_localize_images();
//     global $post;
//     //_log($atts);
//     if(!empty($atts['ids'])){
//         if(empty($atts['orderby'])) $atts['orderby'] = 'post__in';
//         $atts['include'] = $atts['ids'];
//     }
//     // _log(wp_get_attachment_image_src($atts[0]['id']));
//     extract(shortcode_atts(array(
//         'orderby' => 'menu_order ASC, ID ASC',
//         'include' => '',
//         'id' => $post->ID,
//         'itemtag' => 'dl',
//         'icontag' => 'dt',
//         'captiontag' => 'dd',
//         'columns' => 3,
//         'size' => 'thumbnail',
//         'link' => 'file'
//     ), $atts));
//     $args = array(
//         'post_type' => 'attachment',
//         'post_status' => 'inherit',
//         'post_mime_type' => 'image',
//         'orderby' => $orderby
//     );
//     if(!empty($include)) $args['include'] = $include;
//     else{
//         $args['post_parent'] = $id;
//         $args['numberposts'] = -1;
//     }
//     $images = get_posts($args);
//     $gallery = '<div class="gallery-thumbs">';
//     $popup = '<div class="gallery-popup">';
//     $popup .= '<a class="close" href="' . get_permalink() .'"><i class="fa fa-times"></i></a>';
//     $popup .= '<span class="info"><i class="info-button"></i></span>';
// 
//     foreach($images as $image){
//         $caption = $image->post_excerpt;
//         $description = $image->post_content;
//         $title = $image->post_title;
//         // if($description == '') $description = $image->post_title;
//         $image_alt = get_post_meta($image->ID, '_wp_attachment_image_alt', true);
//         $popup .= '<div id="' . $image->ID . '" class="full-image">';
//         if($description){
//             $popup .= '<div class="description">';
//             $popup .= ($title) ? '<h2>' . $title . '</h2>' : '';
//             $popup .= '<p>' . $description . '</p>';
//             $popup .= '</div>';
//         }
//         $popup .= '</div>';
//         // render gallery
//         // $img_size_srcs[] = wp_get_attachment_image_src($image->ID, array('large','medium'));
//         // echo wp_get_attachment_image($image->ID, $size);
//         $thumb = wp_get_attachment_image($image->ID, $size);
//         // $img_src = wp_get_attachment_image_src($image->ID, $size);
//         // $thumb = '<img src="' . $img_src[0] . '"/>';
//         $default_image = wp_get_attachment_image_src($image->ID, 'large');
//         $gallery .= '<a href="' . $default_image[0] . '" class="no-ajaxy">' . $thumb . '</a>';
//     }
//     $gallery .= '</div>';
//     $popup .= '</div>';
//     //echo $gallery;
//     //echo $popup;
//     return $gallery . $popup;
// }

// Main navigation classes
add_filter('nav_menu_css_class', 'fix_nav_parent', 10, 2);
function fix_nav_parent($classes, $item){
    switch(get_post_type()){
        case 'still':
            // _log('post type');
            // _log($classes[2]);
            $title = strtolower($item->title);
            $classes = str_replace('current_page_parent', '', $classes);
            if($title == 'still'){
                array_push($classes, 'current_page_parent', 'current-menu-item');
            }
            break;
        case 'motion':
            $title = strtolower($item->title);
            $classes = str_replace('current_page_parent', '', $classes);
            if($title == 'motion'){
                array_push($classes, 'current_page_parent', 'current-menu-item');
            }
            break;
        case 'post':
            $title = strtolower($item->title);
            if($title == 'news'){
                array_push($classes, 'current-menu-item');
            }
    }
    return $classes;
}

// Function to add close button to main nav.
// add_filter( 'wp_nav_menu_items', 'add_close_button', 10, 2);
// function add_close_button($menu, $args){
//     _log(array('this is $args', $args));
//     _log($menu);
//     if('main-nav' != $args->theme_location) return $menu;
//     switch(get_post_type()){
//         case 'still':
//             $close = build_close_button('/still/');
//             $menu .= $close;
//             break;
//         case 'motion':
//             $close = build_close_button('/motion/');
//             $menu .= $close;
//             break;
//         case 'post':
//             $close = build_close_button('/news/');
//             $menu .= $close;
//     }
//     return $menu;
// }
// function build_close_button($href){
//     return '<li id="close-button"><a href= "'. esc_url(home_url($href)) .'"></a></li>';
// }

// add_filter( 'wp_nav_menu_items', 'add_close_button', 10, 2);
// function add_close_button($menu, $args){
//     if('main-nav' != $args->theme_location) return $menu;
//     $close = '<li id="close-button"><a href="'. home_url() .'"></a></li>';
//     return $menu . $close;
// }

// Disable comments on attachments
function filter_media_comment_status( $open, $post_id ) {
	$post = get_post( $post_id );
	if( $post->post_type == 'attachment' ) {
		return false;
	}
	return $open;
}
add_filter( 'comments_open', 'filter_media_comment_status', 10 , 2 );

// Hack to make gallery thumbs link to 'large' size instead of 'full'
function snm_get_attachment_link_filter( $content, $post_id, $size, $permalink ) {
    // Only do this if we're getting the file URL
        if (! $permalink) {
            // This returns an array of (url, width, height)
            $image = (wp_is_mobile() ? wp_get_attachment_image_src( $post_id, 'medium' ) : wp_get_attachment_image_src($post_id, 'large'));
            $new_content = preg_replace('/href=\'(.*?)\'/', 'href=\'' . $image[0] . '\'', $content );
            return $new_content;
        }
    }
add_filter('wp_get_attachment_link', 'snm_get_attachment_link_filter', 10, 4);
?>
